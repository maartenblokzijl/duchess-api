package nl.api.duchess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class DuchessAPI {

    public static void main(final String... args) {
        SpringApplication.run(DuchessAPI.class, args);
    }
}
