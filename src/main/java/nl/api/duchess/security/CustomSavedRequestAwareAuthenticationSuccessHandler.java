package nl.api.duchess.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CustomSavedRequestAwareAuthenticationSuccessHandler
        extends SimpleUrlAuthenticationSuccessHandler {

    private RequestCache requestCache = new HttpSessionRequestCache();

    // this uses the same logic as the super class' method, but does not redirect
    @Override
    public void onAuthenticationSuccess(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final Authentication authentication
    ) {
        final SavedRequest savedRequest
                = requestCache.getRequest(request, response);

        if (savedRequest == null) {
            super.clearAuthenticationAttributes(request);
            return;
        }
        final String targetUrlParam = super.getTargetUrlParameter();
        if (super.isAlwaysUseDefaultTargetUrl()
                || (targetUrlParam != null
                && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
            this.requestCache.removeRequest(request, response);
            super.clearAuthenticationAttributes(request);
            return;
        }

        super.clearAuthenticationAttributes(request);
    }
}
