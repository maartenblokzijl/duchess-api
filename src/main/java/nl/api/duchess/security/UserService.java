package nl.api.duchess.security;

import lombok.AllArgsConstructor;
import nl.api.duchess.model.Role;
import nl.api.duchess.model.User;
import nl.api.duchess.model.UserRegistration;
import nl.api.duchess.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.concurrent.CompletableFuture;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    @Async
    public CompletableFuture<User> registerNewUser(final UserRegistration user) {
        if (userRepository.findOneByUsername(user.getUsername()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "username " + user.getUsername() + " not available");
        }
        return CompletableFuture.supplyAsync(() -> userRepository.save(
                User.builder()
                        .username(user.getUsername())
                        .password(encoder.encode(user.getPassword()))
                        .role(Role.USER)
                        .build()
        ));
    }
}
