package nl.api.duchess.controller;

import lombok.AllArgsConstructor;
import nl.api.duchess.model.UserRegistration;
import nl.api.duchess.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.async.DeferredResult;

@Controller
@AllArgsConstructor
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    @PostMapping("/api/user/registration")
    public DeferredResult<ResponseEntity> registerUser(
            @RequestBody final UserRegistration userRegistration,
            final DeferredResult<ResponseEntity> deferred
    ) {
        LOG.debug("Received userRegistration: {}", userRegistration);
        userService.registerNewUser(userRegistration)
                .thenApply(ResponseEntity::ok)
                .thenApply(deferred::setResult)
                .exceptionally(deferred::setErrorResult);
        return deferred;
    }
}
